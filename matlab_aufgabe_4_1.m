% Matlab Aufgabe 4, Stefan Heiden, se18m022@technikum-wien.at

% ---------------------------------------------------------------------- %

% BEISPIEL 1

sigma = 55;

curr = imread("images/0000.jpg");

for idx=0:299
    filename = sprintf("images/%04d.jpg", idx);
    prev = curr;
    curr = imread(filename);    
    if (idx > 0)
        delta_masks = (abs(curr - prev)) > sigma;
        
        delta_mask = delta_masks(:,:,1) | delta_masks(:,:,2) | delta_masks(:,:,3);        
        
        marked = overlay(curr, delta_mask);
        imshow(marked, 'InitialMagnification', 500);
    else
        imshow(curr);
    end    
    clf;
end



function [result_img] = overlay(img,mask)	
%
% function [result_img] = overlay(img,mask).
% ------------------------------------------------------------------
% Wandelt das Bild <img> in ein Grauwertbild um und markiert die in
% <mask> angegebenen Pixel rot. Das Resultat wird zur�ckgegeben

    result_img_r = rgb2gray(img);
    result_img_g = rgb2gray(img);
    result_img_b = rgb2gray(img);
    result_img_r(mask) = 255;
    result_img_g(mask) = 0;
    result_img_b(mask) = 0;
    
    result_img(:,:,1) = result_img_r;
    result_img(:,:,2) = result_img_g;
    result_img(:,:,3) = result_img_b;
end