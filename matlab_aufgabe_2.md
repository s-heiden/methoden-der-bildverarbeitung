# Methoden der Bildverarbeitung

2018-09-22, Stefan Heiden, se18m022@technikum-wien.at

## Matlab Aufgabe 2

### Beispiel 1: Kamera Pipeline

Dieses mal ist es die Aufgabe drei Farbbilder vom Rohzustand direkt nach der Erfassung des Sensors in einen für den Menschen ansprechenden Zustand, also in ein digitales Farbbild, umzuwandeln.

Die Input-Daten, drei RAW-Bilder gespeichert in Dateien des Typs ``.tiff``, haben jeweils auf der längeren Seite 1738 Pixel und auf der kürzeren 1158. Im Rohzustand ist jeder Pixel durch eine vorzeichenlose Ganzzahl der Länge 16 Bits repräsentiert.

Lässt man sich diese Input-Daten per `imshow` anzeigen, sieht das Ergebnis wie folgt aus:

![](./matlab_aufgabe_2_1.png)

#### Auslesen relevanter Informationen aus dem Header

Mit dem Befehl `iminfo(<filename>)` lassen sich die Meta-Informationen des Headers anzeigen. Im Fall des `IMG_1.tiff` werden folgende Informationen ausgegeben.

```  
info = struct with fields:
                     Filename: './IMG_1.tiff'
                  FileModDate: '06-Okt-2018 09:56:47'
                     FileSize: 3339305
                       Format: 'tif'
                FormatVersion: []
                        Width: 1738
                       Height: 1158
                     BitDepth: 16
                    ColorType: 'grayscale'
              FormatSignature: [77 77 0 42]
                    ByteOrder: 'big-endian'
               NewSubFileType: 0
                BitsPerSample: 16
                  Compression: 'Deflate'
    PhotometricInterpretation: 'BlackIsZero'
                 StripOffsets: [1×145 double]
              SamplesPerPixel: 1
                 RowsPerStrip: 8
              StripByteCounts: [1×145 double]
                  XResolution: []
                  YResolution: []
               ResolutionUnit: 'Inch'
                     Colormap: []
          PlanarConfiguration: 'Chunky'
                    TileWidth: []
                   TileLength: []
                  TileOffsets: []
               TileByteCounts: []
                  Orientation: 1
                    FillOrder: 1
             GrayResponseUnit: 0.0100
               MaxSampleValue: 65535
               MinSampleValue: 0
                 Thresholding: 1
                     Offset: 8
                 SampleFormat: 'Unsigned integer'
                   BlackLevel: 1439
                AsShotNeutral: [0.4183 1 0.7161]
```

BlackLevel, das den Schwarzwert der Kamera bezeichnet und der aufgrund von Rauschen nicht bei 0 liegt, beträgt im ersten Fall 1439. AsShotNeutral bezeichnet den Farbwert von “Weiß”. Dieser ist durch die RGB-Werte [0.4183 1 0.7161] definiert.

#### Abbildung des Intervalls [BlackLevel, 65535] auf das Interval [0.0, 1.0]
Mein erster Ansatz war es hier, eine Funktion zu schreiben, die für jeden Pixel des RAW-Bildes 0 zurückgibt, falls die Intensität des Pixels, x, <= `blackLevel` beträgt und andernfalls `(x - blackLevel) / (maxIntensity - blackLevel)`, wobei gilt `maxIntensity := 65535`.

Die vollständige Funktion lautet:

```
function [y] = spread(x, blackLevel, maxIntensity)
    y = (x - blackLevel > 0) * (x - blackLevel) / (maxIntensity - blackLevel);
end
```

Diese wird nun mittels `arrayfun(@(intensity) spread(intensity, info.BlackLevel, maxIntensity), I)` aufgerufen, um sie für jeden Pixel des Inputs durchzuführen. Es zeigte sich, dass dieser Ansatz zu sehr langen Verarbeitungszeiten führte.

Ein leistungsfähigerer Ansatz ist es, elementweise Operationen zu verwenden, um dieselbe Logik abzubilden. Meine Lösung dafür ist: `I = double(Input - info.BlackLevel > 0) .* double(Input - info.BlackLevel) ./ double(65535 - info.BlackLevel);`
    
Auch die eingebaute Matlab-Funktion `im2double(<img>)` kann für diesen Anwendungsfall genutzt werden.

Lässt man sich diese auf ein Intervall gemappten Daten per `imshow` anzeigen, sieht das Ergebnis wie folgt aus:

![](./matlab_aufgabe_2_2.png)

#### Demosaicing

Für das Demosaicing ging ich lt. den Anweisungen der Angabe vor:

> Der Bildsensor ist mit einem Farbfilter (Bayer Pixel Pattern) überzogen, der nur eine bestimmte Farbkomponente zum Sensor durchlässt. Diese Filter sind in 2x2 Pixelmustern angeordnet. Da die Sensitivität des menschlichen Auges im grünen Bereich am besten ist, wird der grüne Filter auf zwei der vier Sensoren aufgetragen. Verschiedene Kameramodelle verwenden verschiedene Muster, in unserem Fall wurde folgender Filter verwendet: `[B][G][G][R]`
 
> Dieses Muster wird beginnend vom linken, oberen Bildpunkt über dem ganzen Bild wiederholt. Um alle drei Farbkanäle für jeden Pixel zu bekommen, müssen an den Stellen, an denen Informationen fehlen, die benachbarten Werte interpoliert werden, wie auf der folgenden Abbildung verdeutlicht:

![](./matlab_aufgabe_2_3.png)

> Um die fehlenden Blau- und Rotwerte zu berechnen wird das Mittel der angrenzenden 2 oder 4 Werte bestimmt. Für die grünen Werte müssen jeweils die vier Nachbarn gemittelt werden. Nachdem alle 3 Kanäle über das ganze Bild bestimmt wurden, fügen Sie alles zu einem einzigen RGB-Bild zusammen. Hinweis: Der demosaic Befehl in Matlab verwendet ein komplizierteres Interpolationsverfahren. Er kann als Vorschau helfen, darf jedoch in der Lösung nicht verwendet werden.

> **Vorgehensweise**: Erstellen Sie sich zuerst Matrizen für den Rot-, Grün- und Blaukanal und füllen Sie diese mit den entsprechenden Werten aus dem Bild I.

Hierfür habe ich in meinem Skript folgende Anweisungen definiert:

Definition eines Patterns, der die Verteilung der Farbinformationen im kleinsten wiederkehrenden Bereich des Sensor-Inputs (2 x 2 Pixel) darstellt. Dabei ist blau durch `3`, grün durch `2` und rot durch `1` dargestellt.

```
pattern = [3 2; 2 1];
```

Definition einer Maske, `bayer`, mit derselben Größe wie das Ausgangsbild, welche mit `pattern` befüllt wird.

```
bayer = repmat(pattern, size(I,1)/2, size(I,2)/2);
```

Erstellung von mit Nullen gefüllten Matrizen `R`, `G`, `B` mit den Dimensionen von `I` mit der Funktion `zeros(<Matrix>)`.

Im Fall von Rot: Zuweisung der Werte von `I` an deren Position in `bayer` eine `1` steht, an dieselbe Position von `R`. Im Fall von grün und blau, äquivalent mit den Ziffern `3` und `2`. Dies wird mit folgenden Anweisungen erreicht:

```
R(bayer==1) = I(bayer==1);
G(bayer==2) = I(bayer==2);
B(bayer==3) = I(bayer==3);
``` 

Um die fehlenden Werte zu interpolieren, werden die in der Angabe definierten Filter-Matrizen in Verbindung mit der Funktion `imfilter(<Matrix>)` genutzt.

Für das Demosaicing ergibt sich also folgender Programmcode:

```
pattern = [3 2; 2 1];
bayer = repmat(pattern, size(I,1)/2, size(I,2)/2);

R = zeros(size(I));
G = zeros(size(I));
B = zeros(size(I));

R(bayer==1) = I(bayer==1);
G(bayer==2) = I(bayer==2);
B(bayer==3) = I(bayer==3);

h = 0.5;
q = 0.25;
RBFilter = [q h q; h 1 h; q h q];
GFilter  = [0 q 0; q 1 q; 0 q 0];

R = imfilter(R, RBFilter);
G = imfilter(G, GFilter);
B = imfilter(B, RBFilter);
```

Führt man diese drei Farbkanäle mit der Funktion `cat` zu einem zusammen, ergibt sich folgendes Bild.

![](./matlab_aufgabe_2_4.png)

#### Weißabgleich

Für den Weißabgleich werden die Intensitäten jedes Farbkanals durch den Wert `AsShotNeutral` dividiert. Werte `>= 1` werden zu `1` korrigiert.

Auch hier habe ich zunächst den langsameren Ansatz gewählt, eine Funktion `whiteadjust` zu schreiben, die mittels `arrayfun` für alle Positionen der Matrix aufgerufen wird.

```
R = arrayfun(@(intensity) whiteadjust(intensity, info.AsShotNeutral(1)), R);

function [y] = whiteadjust(x, asShotNeutral)
    y = (x / asShotNeutral > 1 ) * 1 + (x / asShotNeutral <= 1) * x / asShotNeutral;
end
```

Wesentlich performanter und eleganter lässt sich dies mit elementweisen Operationen lösen:

```
R = (R / info.AsShotNeutral(1) > 1 ) .* 1 + (R / info.AsShotNeutral(1) <= 1) .* R / info.AsShotNeutral(1);
```

#### Adjustieren des Kontrasts

Um eine Informationsbasis für das optimale Adjustieren der Bilder zu bekommen, nutzte ich die Funktion `histogram` um mir die Verteilung der Intensitäten in den unterschiedlichen Kanälen anzusehen.

![](./matlab_aufgabe_2_5.png)

Nach einigem Herumprobieren erhielt ich die besten Resultate, wenn ich zunächst für alle Kanäle `imadjust` aufrief und diese anschließend bloß mittels `cat` zusammenführte.

Dabei erhielt ich folgende Ergebnisse:

![](./matlab_aufgabe_2_6.png)

