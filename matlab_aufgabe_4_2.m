% Matlab Aufgabe 4, Stefan Heiden, se18m022@technikum-wien.at

% ---------------------------------------------------------------------- %

% BEISPIEL 2

curr = imread("images/0000.jpg");

bg_model = zeros(size(curr));

model_min = 0; 
model_max = 80;

for idx = model_min:model_max
    filename = sprintf("images/%04d.jpg", idx);
    curr = imread(filename);    
    bg_model = double(curr) + bg_model;
end

bg_model = bg_model / double(model_max - model_min);

input_min = 81;
input_max = 299;
sigma = 28;

for idx = input_min:input_max
    filename = sprintf("images/%04d.jpg", idx);
    curr = imread(filename);
    delta_masks = (abs(double(curr) - bg_model)) > sigma;
    delta_mask = delta_masks(:,:,1) | delta_masks(:,:,2) | delta_masks(:,:,3);
    marked = overlay(curr, delta_mask);
    imshow(marked, 'InitialMagnification', 500);   
    clf;
end



function [result_img] = overlay(img,mask)	
%
% function [result_img] = overlay(img,mask).
% ------------------------------------------------------------------
% Wandelt das Bild <img> in ein Grauwertbild um und markiert die in
% <mask> angegebenen Pixel rot. Das Resultat wird zur�ckgegeben

    result_img_r = rgb2gray(img);
    result_img_g = rgb2gray(img);
    result_img_b = rgb2gray(img);
    result_img_r(mask) = 255;
    result_img_g(mask) = 0;
    result_img_b(mask) = 0;
    
    result_img(:,:,1) = result_img_r;
    result_img(:,:,2) = result_img_g;
    result_img(:,:,3) = result_img_b;
end