# Methoden der Bildverarbeitung
2018-09-22, Stefan Heiden, se18m022@technikum-wien.at
## Matlab Aufgabe 1
### Beispiel 1 - Matlab und Bildverarbeitung

#### Protokoll

- Zunächst wird das Bild butterfly.jpg mittels dem Befehl ``imread`` in eine 600-mal-450-mal-3-Matrix geladen.
- Dann wird das Bild um 90 Grad im Uhrzeigersinn rotiert. Da ``imrotate`` per default gegen den Uhrzeigersinn rotiert, wird ein negativer Betrag angegeben.
- Im nächsten Schritt werden die ersten 150 Zeilen nach unten gesetzt. Die Funktion ``circshift`` kann die Werte von Matrizen verschieben. In diesem Fall sollen alle Zeilen, zirkulär um 150 Zeilen nach oben verschoben werden, sodass die obersten 150 - in der richtigen Reihenfolge - nach unten gelangen. Daher lautet der Aufruf ``circshift(rotated, [-150 0])``.
- Mit der Syntax ``<Image>(:, :, n)``, mit ``n`` zwischen 1 und 3, wurden die einzelnen Farbkanäle (rot, grün, blau) aus dem Image gewonnen und anschließend mit der Funktion ``imshow`` dargestellt.
- Anschließend wurde die Funktion ``rgb2gray`` genutzt, um das Bild in ein Graustufenbild zu konvertieren.

#### Untersuchen sie mithilfe des entsprechenden Eintrags im Workspace oder dem Befehl whos, wieviel Speicher das Bild nach dem Laden belegt. Wie ergibt sich diese Zahl aus der räumlichen und radiometrischen Aufösung des Bildes?

- Nach dem Laden belegt das Bild 810.000 Bytes.
- Dies ergibt sich aus dem Umstand, dass ein Farbwert durch ein unsigned Integer der Länge 1 Byte repräsentiert wird.
- Das Bild wird als dreidimensionale Matrix modelliert.
- Insgesamt werden drei Farbkanäle für eine Fläche von 600 mal 450 Pixeln repräsentiert.
- 600 x 450 x 3 x 1 ergibt 810.000 Byte.

#### Finden Sie heraus, mithilfe welcher Formel die Umwandlung von RGB in ein Graustufenbild mittels rgb2gray funktioniert. Nutzen Sie dazu die Matlab-Hilfe des Befehls.

> rgb2gray converts RGB values to grayscale values by forming a weighted sum of the R, G, and B components:
> 0.2989 * R + 0.5870 * G + 0.1140 * B

Diese Formel entspricht der Empfehlung der International Telecommunications Union für die Konvertierung in Graustufen veröffentlicht unter [https://www.itu.int/dms_pubrec/itu-r/rec/bt/R-REC-BT.601-7-201103-I!!PDF-E.pdf](https://www.itu.int/dms_pubrec/itu-r/rec/bt/R-REC-BT.601-7-201103-I!!PDF-E.pdf).

### Beispiel 2 - Matlab-Operationen
#### Gegeben ist folgender Matlab-Code:
```
A = 1:0.1:10;
B = 1:0.1:10;
for i=1:length(A)
	result(i)=A(i)*B(i);
	result(i)=log(result(i));
end
figure;
plot(A,result);
```
#### Geben sie diesen Code in ein Skript-File ein und führen Sie es aus. Was ist das Resultat? Versuchen Sie nachzuvollziehen, was im Code gemacht wird. Ihre Aufgabe ist es, die for-Schleife im Code durch eine einzige Zeile zu ersetzen, ohne etwas an der Funktionalität des Programms zu ändern. Sie benötigen dazu die elementweisen arithmetischen Operatoren von Matlab.

- Der Code ``1:0.1:10`` erstellt eine 1-x-91-Matrix, gefüllt mit dem Intervall von 1, 1.1, 1.2, 1.3, ... bis einschließlich 10.
- Es werden zwei Matrizen, ``A`` und ``B``, mit diesem Inhalt definiert.
- In der For-Schleife wird eine weitere 1-x-91-Matrix, ``result``, gefüllt.
- Dabei gilt, jede Spalte von ``result`` enthält den Logarithmus des Produkts der Werte von ``A`` und ``B``.
- Es wird anschließen mit ``figure`` ein Darstellungsfenster geöffnet, worin mit der Funktion ``plot`` eine Funktion gezeichnet wird.
- Dabei gibt das erste Argument, ``A``, die x-Werte an, für die im Folgenden Funktionswerte definiert werden sollen. Diese sind im zweiten Argument, ``result``, angeführt.
- Die For-Schleife kann durch den Ausdruck ``result = log(A.*B);`` ersetzt werden. Dabei wird der Logarithmus aus dem Ergebnis der elementweisen Multiplikation von A und B ermittelt.


