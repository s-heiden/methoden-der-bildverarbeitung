% Matlab Aufgabe 3, Stefan Heiden, se18m022@technikum-wien.at

% ---------------------------------------------------------------------- %

% BEISPIEL 3.1

% read from input file
I = imread('./obama.gif');

I = imadjust(I);

% use myedge
E = myedge(I, 3);


function E = myedge(I, THRESH)

    %
    % INPUTS
    % I        Grauwertbild
    % THRESH   Schwellwert; alle Gradientenbetr�ge kleiner als THRESH werden
    %          ignoriert.
    % OUTPUTS
    % E        Bin�res Kantenbild, selbe Gr��e wie I
    gaussianFilter = fspecial('gaussian', 5, 2);
    IFilteredGaussian = imfilter(I, gaussianFilter);

    % 1.
    % zun�chst erstelle ich mit fspecial einen Gauss'schen Filter mit der
    % Kernelgr��e 2 und der Standardabweichung (Sigma) 2. Diesen wende ich
    % dann auf das Inputbild I an.

    % 2.
    % Es ist nun die Aufgabe, dx und dy zu erstellen, 2 Bilder bei denen jedes
    % Pixel die erste Ableitung (nach x oder y abgeleitet) des jeweiligen
    % Pixels im Ursprungsbild an derselben Stelle, repr�sentiert.
    % Dazu wird zun�chst der Sobel-Filter f�r die erste Ableitung in
    % der x-Richtung und der y-Richtung definiert:

    %sobelOperatorX = [1 2 1; 0 0 0; -1 -2 -1];
    %sobelOperatorY = [1 0 -1; 2 0 -2; 1 0 -1];
    
    % Eine einfachere L�sung ist die Verwendung des eingebauten Funktion
    % fspecial, die den Filter f�r die X-Koordinate zur�ckgibt. Dieser kann
    % invertiert werden um vertikale Kanten zu erkennen.
    
    sobelOperatorX = fspecial('sobel');
    sobelOperatorY = sobelOperatorX';

    % Mit imfilter werden die Operatoren nun angewendet:
    dx = imfilter(IFilteredGaussian, sobelOperatorX);
    dy = imfilter(IFilteredGaussian, sobelOperatorY);


    
    % 3.
    % Nun gilt es, den Gradientenbetrag (Magnitude of the Gradient) G zu 
    % berechnen. Dieser ist f�r jeden Pixel definiert durch die L�nge jenes 
    % 2D-Vektor, der aus den Werten dx und dy im jeweiligen Pixel des 
    % Inputbilds resultiert.
    
    
    G = sqrt(double(dx .^ 2 + dy .^ 2));
    
    % 4.
    % Nun soll der invertierte Gradientenbetrag gemeinsam mit dem
    % Vektorfeld (dy, dx) angezeigt werden.
      
    % 5.
    % Im n�chsten Schritt wird ein bin�res Kantenbild erstellt.
    % Dazu wird der Eingabeparameter THRESH als Schwellwert
    % f�r den Gradientenbetrag, G, genutzt, um aus dem 
    % Grauwertbild ein Bin�rbild, B, zu erzeugen. Dazu nutze ich
    % die Funktion imbinarize, die wie folgt dokumentiert ist:
    %
    % 'BW = imbinarize(I,T) creates a binary image from image I using the
    % threshold value T. T can be a global image threshold, specified as a 
    % scalar luminance value, or a locally adaptive threshold, specified as 
    % a matrix of luminance values.'
    % Quelle: https://de.mathworks.com/help/images/ref/imbinarize.html
    
    B = imbinarize(G, THRESH);
   
    IThinned = zeros(size(B));    
    
    xMax = size(B,1);
    yMax = size(B,2);
    
    for x = 2:xMax-1
        for y = 2:yMax-1
            
            if B(x,y) > 0
                
                b = G(x,y);

                x1 = round(x + double(dx(x,y)/b));
                y1 = round(y + double(dy(x,y)/b));
                x2 = round(x - double(dx(x,y)/b));
                y2 = round(y - double(dy(x,y)/b));
                
                if (x1 > xMax || y1 > yMax || x2 < 1 || y2 < 1)
                    continue
                end
                

                if G(x,y) < G(x1,y1) || G(x,y) < G(x2,y2)
                    
                    IThinned(x,y) = 0;
                else
                    IThinned(x,y) = B(x,y);
                end

            end
        end
    end
    
    % TODO: Fragenbeantwortung

    set(gcf,'color','white')
    figure(1);
    subplot(2,4,1)
    imshow(I)
    title('Input');
    subplot(2,4,2)
    imshow(IFilteredGaussian)
    title('Input nach Anw. des Gau�schen Filters')
    subplot(2,4,3)
    imshow(dx)
    title('dx')
    subplot(2,4,4)
    imshow(dy)
    title('dy')
    subplot(2,4,5)
    imshow(G)
    title('Gradientenbetrag');
    hold on
    quiver(dy, dx);
    subplot(2,4,6)
    imshow(B)
    title('Bin�res Gradientenbetragsbild nach Anwendung der Schwelle')
    subplot(2,4,7);
    imshow(IThinned);
    title('Verd�nntes Bin�rbild');
    
    E = IThinned;
end
