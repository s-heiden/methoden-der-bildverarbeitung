% Matlab Aufgabe 2, Stefan Heiden, se18m022@technikum-wien.at

% ---------------------------------------------------------------------- %

% BEISPIEL 1

filenames = ["IMG_1.tiff" "IMG_2.tiff" "IMG_3.tiff"];
results = cell(3, 1);

% Auslesen von Schwarzwert und den Wei�-Farbwert aus dem Header
for i = 1:1:3
    filename = filenames(i);
    info = imfinfo(filename); 
    Input = imread(filename);
        
    maxIntensity = 65535;

    % Abbildung des Intervall [BlackLevel, 65535] auf das Interval [0.0, 1.0]
    % A. using inbuilt function
    % I = im2double(Input);    
    
    % B. using element-wise operations    
    I = double(Input - info.BlackLevel > 0) .* double(Input - info.BlackLevel) ./ double(65535 - info.BlackLevel);
    
    imshow(I);
    
    % C. using arrayfun in conjunction with custom function     
    % I = arrayfun(@(intensity) spread(intensity, info.BlackLevel, maxIntensity), I);
    
    % Demosaicing
    pattern = [3 2; 2 1];
    bayer = repmat(pattern, size(I,1)/2, size(I,2)/2);

    R = zeros(size(I));
    G = zeros(size(I));
    B = zeros(size(I));

    R(bayer==1) = I(bayer==1);
    G(bayer==2) = I(bayer==2);
    B(bayer==3) = I(bayer==3);

    h = 0.5;
    q = 0.25;
    RBFilter = [q h q; h 1 h; q h q];
    GFilter  = [0 q 0; q 1 q; 0 q 0];

    R = imfilter(R, RBFilter);
    G = imfilter(G, GFilter);
    B = imfilter(B, RBFilter);
    
    imshow(cat(3, R, G, B));

    % White adjust    
    % slower alternative:
    % R = arrayfun(@(intensity) whiteadjust(intensity, info.AsShotNeutral(1)), R);
    % G = arrayfun(@(intensity) whiteadjust(intensity, info.AsShotNeutral(2)), G);
    % B = arrayfun(@(intensity) whiteadjust(intensity, info.AsShotNeutral(3)), B);
    R = (R / info.AsShotNeutral(1) > 1 ) .* 1 + (R / info.AsShotNeutral(1) <= 1) .* R / info.AsShotNeutral(1);
    G = (G / info.AsShotNeutral(2) > 1 ) .* 1 + (G / info.AsShotNeutral(2) <= 1) .* G / info.AsShotNeutral(2);
    B = (B / info.AsShotNeutral(3) > 1 ) .* 1 + (B / info.AsShotNeutral(3) <= 1) .* B / info.AsShotNeutral(3);
    
    % Collecting information for imadjust
    % figure, histogram(R);
    % figure, histogram(G);
    % figure, histogram(B);
    
    R = imadjust(R);
    G = imadjust(G);
    B = imadjust(B);
    
    results{i} = cat(3, R, G, B);
end

% figure, histogram(results{1});
% figure, histogram(results{2});
% figure, histogram(results{3});

% RGB1 = imadjust(results{1}, [0.025 0.175], [0.01 0.75]);
% RGB2 = imadjust(results{2}, [0.025 0.4], [0.01 0.8]);
% RGB3 = imadjust(results{3}, [0.025 0.3], [0.01 0.8]);

RGB1 = results{1};
RGB2 = results{2};
RGB3 = results{3};

figure, imshow(RGB1)
figure, imshow(RGB2)
figure, imshow(RGB3)

% function [y] = spread(x, blackLevel, maxIntensity)
%     y = (x - blackLevel > 0) * (x - blackLevel) / (maxIntensity - blackLevel);
% end

% function [y] = whiteadjust(x, asShotNeutral)
%     y = (x / asShotNeutral > 1 ) * 1 + (x / asShotNeutral <= 1) * x / asShotNeutral;
% end
