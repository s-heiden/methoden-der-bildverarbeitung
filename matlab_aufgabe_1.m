% Matlab Aufgabe 3, Stefan Heiden, se18m022@technikum-wien.at

% ---------------------------------------------------------------------- %

% AUFGABE 3.1

% read from input file
input = imread('./butterfly.jpg');

% rotate by 90 degrees clockwise
rotated = imrotate(input, -90);

% shift the first 150 rows of the matrix down
shifted = circshift(rotated, [-150 0]);

% generate images which contain only the red, green, blue channel
channel_red   = shifted(:, :, 1);
channel_green = shifted(:, :, 2);
channel_blue  = shifted(:, :, 3);

% show images
imshow([channel_red, channel_green, channel_blue]);

% convert to grayscale
converted_to_grayscale = rgb2gray(shifted);

% write to file
imwrite(converted_to_grayscale, 'butterfly_result.jpg');

% ---------------------------------------------------------------------- %

% BEISPIEL 2

A = 1:0.1:10;
B = 1:0.1:10;
result = log(A.*B);
figure;
plot(A,result);

% ---------------------------------------------------------------------- %
